const mix = require('laravel-mix');

mix.setPublicPath('public/dist')
.sass('view/assets/css/app.scss','css')
.js('view/assets/js/app.js','js')