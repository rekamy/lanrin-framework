<?php

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Connection;

class DB extends QueryBuilder
{
    // public function __construct()
    // {
    //     $connectionParams = array(
    //         'dbname' => 'systemposuni',
    //         'user' => 'root',
    //         'password' => '',
    //         'host' => 'localhost',
    //         'driver' => 'mysqli',
    //     );
    //     $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
    //     return $conn->createQueryBuilder();
    // }



    public function __construct()
    {
        $connectionParams = array(
            'dbname' => 'systemposuni',
            'user' => 'root',
            'password' => '',
            'host' => 'localhost',
            'driver' => 'mysqli',
        );
        $conn = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
        $this->connection = $conn->createQueryBuilder();

        dump($this->connection);

    }
    
    public function instance(){
        return $this->connection;
    }

    public function get()
    {
        return $this->execute()->fetchAllAssociative();
    }
}
