<?php

function view(String $page)
{
    $files =  __DIR__ . "/../../view/" . $page . ".php";

    if (file_exists($files)) {
        // $renderhtmlcontent = file_get_contents($files);

        
        $layout =  __DIR__ . "/../../view/layout/index.php";
        
        include $layout;
    } else {
        dd("Ooops file not exist!");
    }
}


